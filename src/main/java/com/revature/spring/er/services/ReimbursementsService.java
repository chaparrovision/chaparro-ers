package com.revature.spring.er.services;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.revature.spring.er.repositories.ReimbursementsRepository;
import com.revature.spring.er.beans.ReimbursementsBean;


@Service
public class ReimbursementsService {
	@Autowired
	EntityManager em;
	
	ReimbursementsRepository rr;
	
	public ReimbursementsService(ReimbursementsRepository rr) {
		super();
		this.rr = rr;
	}
	public ReimbursementsBean createReimbursement(ReimbursementsBean r) {
		return rr.save(r);
	}
	
	@Transactional(propagation  = Propagation.NEVER)
	public List<ReimbursementsBean> getReimbursement() {
		return rr.findAll();
	}
	
}
