package com.revature.spring.er.services;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.revature.spring.er.repositories.UsersRepository;
import com.revature.spring.er.beans.UsersBean;

@Service
public class UsersService {
	@Autowired
	EntityManager em;
	
	UsersRepository ur;
	
	public UsersService(UsersRepository ur) {
		super();
		this.ur = ur;
	}
	public UsersBean createUser(UsersBean u) {
		return ur.save(u);
	}
	
	@Transactional(propagation  = Propagation.NEVER)
	public List<UsersBean> getUser() {
		return ur.findAll();
	}
	
}
