package com.revature.spring.er.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.revature.spring.er.beans.UsersBean;
import com.revature.spring.er.services.UsersService;

@CrossOrigin(origins = "*", allowedHeaders = "*", methods = {RequestMethod.POST,
		RequestMethod.GET, RequestMethod.PUT, RequestMethod.DELETE})

@RestController
@RequestMapping("/user")

public class UsersController {
	@Autowired
	UsersService userservice; // not plural?
	
	// CREATE
	@PostMapping
	public UsersBean createUser(@RequestBody UsersBean user) {
		return userservice.createUser(user);
	} 
	
	// READ
	@GetMapping
	public List<UsersBean> getUser() {
		return userservice.getUser();
	}
}	

/*
 * 	@RequestMapping("/users")
	public List<UsersBean> getAllUsers() {
		return usersRepository.findAll();
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/users")
		public void addUsers (@RequestBody UsersBean users) {
		usersRepository.save(users);
	}
}
 * */

