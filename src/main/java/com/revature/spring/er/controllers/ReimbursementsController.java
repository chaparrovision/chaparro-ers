package com.revature.spring.er.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.revature.spring.er.beans.ReimbursementsBean;
import com.revature.spring.er.services.ReimbursementsService;

@CrossOrigin(origins = "*", allowedHeaders = "*", methods = {RequestMethod.POST,
		RequestMethod.GET, RequestMethod.PUT, RequestMethod.DELETE})

@RestController
@RequestMapping("/reimbursement")

public class ReimbursementsController {
	@Autowired
	ReimbursementsService reimbursementservice; // not plural?
	
	// CREATE
	@PostMapping
	public ReimbursementsBean createReimbursement(@RequestBody ReimbursementsBean reimbursement) {
		return reimbursementservice.createReimbursement(reimbursement);
	} 
	
	// READ
	@GetMapping
	public List<ReimbursementsBean> getReimbursement() {
		return reimbursementservice.getReimbursement();
	}
}	

