package com.revature.spring.er.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.revature.spring.er.beans.UsersBean;

@Repository
public interface UsersRepository extends JpaRepository<UsersBean,Integer>{

}
