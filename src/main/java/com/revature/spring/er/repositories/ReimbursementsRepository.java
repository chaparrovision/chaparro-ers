package com.revature.spring.er.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.revature.spring.er.beans.ReimbursementsBean;

@Repository
public interface ReimbursementsRepository extends JpaRepository<ReimbursementsBean,Integer>{

}
